from config import *
from data import truncate_pad, Lang
from transformer import TransformerEncoder, TransformerDecoder, EncoderDecoder



def predict_seq2seq(net, src_sentence, lang, num_steps,
                    device, save_attention_weights=False):
    def voc(line):
        return [lang.word2idx(i) for i in line]

    #"""Predict for sequence to sequence."""
    # Set `net` to eval mode for inference
    net.eval()
    src_tokens = voc((src_sentence.lower()).split()) + [3]
    enc_valid_len = torch.tensor([len(src_tokens)], device=device)
    # 截断、填充文本序列
    src_tokens = truncate_pad(src_tokens, num_steps, 1)
    # Add the batch axis
    enc_X = torch.unsqueeze(
        torch.tensor(src_tokens, dtype=torch.long, device=device), dim=0)
    enc_outputs = net.encoder(enc_X, enc_valid_len)
    dec_state = net.decoder.init_state(enc_outputs, enc_valid_len)
    # Add the batch axis
    dec_X = torch.unsqueeze(
        torch.tensor([2], dtype=torch.long, device=device),
        dim=0)
    output_seq, attention_weight_seq = [], []
    for _ in range(num_steps):
        Y, dec_state = net.decoder(dec_X, dec_state)

        dec_X = Y.argmax(dim=2)  # 最大的下标，即为最终词下标
        pred = dec_X.squeeze(dim=0).type(torch.int32).item()  # 去掉第一维取结果
        # Save attention weights (to be covered later)
        if save_attention_weights:
            attention_weight_seq.append(net.decoder.attention_weights)
        # Once the end-of-sequence token is predicted, the generation of the
        # output sequence is complete
        if pred == 3:
            break
        output_seq.append(pred)
    return ' '.join([lang.index2word[i] for i in output_seq]), attention_weight_seq


if __name__=="__main__":
    lang = torch.load(DATA_ROOT + "dialog.lang")
    encoder = torch.load(MODEL_ROOT + "trans_encoder4.mdl", map_location=torch.device('cpu'))
    decoder = torch.load(MODEL_ROOT + "trans_decoder4.mdl", map_location=torch.device('cpu'))

    net = EncoderDecoder(encoder, decoder)
    while 1:
        sentence = input()
        out, weight = predict_seq2seq(net, sentence, lang, num_steps, device)
        print(out)

