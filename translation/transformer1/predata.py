from config import DATA_ROOT
import opencc
cc = opencc.OpenCC('t2s')
with open(DATA_ROOT+"eng-cmn.txt", encoding="utf-8") as f1:
    f2 = open(DATA_ROOT + "eng-chi.txt", encoding="utf-8", mode="w+")
    for line in f1:
        f2.write(cc.convert(line))

