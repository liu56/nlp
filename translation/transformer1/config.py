import torch

DATA_ROOT = '../../data/eng-cmn/' # the parent root where your train/val/test data are stored
MODEL_ROOT = '../../model/translation/' # the root to buffer your checkpoints

UNK_IDX, PAD_IDX, BOS_IDX, EOS_IDX = 0, 1, 2, 3
special_symbols = ['<unk>', '<pad>', '<bos>', '<eos>']

MAX_LENGTH = 20
EMBEDDING_SIZE = 256  # feature dimension
LR = 0.01  # initial LR

DEVICE = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")


if __name__=="__main__":
    lines = open(DATA_ROOT+'eng-cmn/eng-cmn.txt', encoding='utf-8').read().strip().split('\n')
    print(lines[0])
    